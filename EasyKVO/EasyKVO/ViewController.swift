//
//  ViewController.swift
//  EasyKVO
//
//  Created by 王智刚 on 2020/9/15.
//  Copyright © 2020 王智刚. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let q = OperationQueue()
        var o = Operation()
        o.completionBlock = {
            print("线程\(Thread.current)")
        }
        DispatchQueue()
        q.addOperation(o)
        q.addBarrierBlock {
            print("barrier线程\(Thread.current)")
        }
        q.addOperation {
            print("线程\(Thread.current)")
        }
        // Do any additional setup after loading the view.
    }
}

