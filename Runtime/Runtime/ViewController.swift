//
//  ViewController.swift
//  Runtime
//
//  Created by 王智刚 on 2020/9/14.
//  Copyright © 2020 王智刚. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let nvc = NewViewController()
        self.addChild(nvc)
        self.view.addSubview(nvc.view)
        
    }
    
    func testMethodRuntime() {
        let oSelector = #selector(Test.handle1)
        let bSelector = #selector(ViewController.viewDidLoad)
        guard let method1 = class_getClassMethod(Test.self, oSelector) else { return }
        guard let method2 = class_getClassMethod(ViewController.self, bSelector) else { return }
        method_exchangeImplementations(method1, method2)
    }
    
    func class_some() {
        
    }
    
    func objc_some() {
        
    }
    
    func ivar_some() {
        
    }
    
    func property_some() {
        
    }
    
    func method_some() {}
    
    func protocol_some() {
        
    }
}

@objcMembers
class Test: NSObject {
    func handle1() {
        var num2 = [1, 2]
        num2.remove(at: 0)
    }
}

