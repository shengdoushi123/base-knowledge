//
//  NewViewController.m
//  Runtime
//
//  Created by 王智刚 on 2020/9/14.
//  Copyright © 2020 王智刚. All rights reserved.
//

#import "NewViewController.h"
//#import "fishhook.h"


@interface Test: NSObject
@property (nonatomic, assign) NSInteger ceshi;

@end

@implementation Test

@end

@interface NewViewController()
@property (nonatomic, strong)NSString *str1;
@property (nonatomic, weak)NSString *str2;

@end

@implementation NewViewController

typedef void (^blk_t)(id obj);
blk_t blk;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self captureObject];
    
    blk([[NSObject alloc] init]);
    
    blk([[NSObject alloc] init]);
    
    blk([[NSObject alloc] init]);
    NSString *str1 = @"测试1";
    Test *obj1 = [[Test alloc] init];
    obj1.ceshi = 3;
    NSLog(@"111%p", obj1);
    NSLog(@"111%d", obj1.ceshi);
    
    void (^blk2)() = [^() {
        NSLog(str1);
        NSLog(@"222%p", obj1);
        NSLog(@"222%d", obj1.ceshi);
    } copy];
    obj1.ceshi = 4;
//    obj1 = [[Test alloc] init];
//    obj1.ceshi = 5;
//    NSLog(@"333%p", obj1);
    blk2();
}

- (void)captureObject
{
    id array = [[NSMutableArray alloc] init];
    blk = ^(id obj) {
        [array addObject:obj];
        NSLog(@"array count = %ld", [array count]);
    };
}

struct __block_impl {
    void *isa;
    int Flags;
    int Reserved;
    void *FuncPtr;
};

struct __main_block_desc_0 {
    size_t reserved;
    size_t Block_size;
};

struct __main_block_impl_0 {
    struct __block_impl impl;
    struct __main_block_desc_0 *Desc;
};

static void (* original_func)(struct __main_block_impl_0 *, int, NSString *);
static id (*original__Block_copy)(id);

id new__Block_copy(id block) {
    id tmp = original__Block_copy(block);
    HookBlockToPrintArguments(tmp);
    
    return tmp;
}

static void test_1__main_block_func_0(struct __main_block_impl_0 *__cself){
    NSLog(@"hello world");
}

static void test_2__main_block_func_0(struct __main_block_impl_0 *__cself, int a, NSString *b){
    NSLog(@"%d, %@", a, b);
    original_func(__cself, a, b);
}

void HookBlockToPrintHelloWorld(id block) {
    struct __block_impl *temp = (__bridge struct __block_impl *)block;
    temp->FuncPtr = &test_1__main_block_func_0;
}

void HookBlockToPrintArguments(id block) {
    struct __block_impl *tmp = (__bridge struct __block_impl *)block;
    original_func = tmp->FuncPtr;
    tmp->FuncPtr = &test_2__main_block_func_0;
}

//void HookEveryBlockToPrintArguments() {
//    struct rebinding open_rebinding = {
//        "_Block_copy",
//        new__Block_copy,
//        (void *)&original__Block_copy
//    };
//
//    rebind_symbols((struct rebinding[1]){open_rebinding}, 1);
//}

@end
