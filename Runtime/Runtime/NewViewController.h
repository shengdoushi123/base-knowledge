//
//  NewViewController.h
//  Runtime
//
//  Created by 王智刚 on 2020/9/14.
//  Copyright © 2020 王智刚. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Father : NSObject

@end

@implementation Father


@end

@interface Person : Father

-(void)setName;

@end

@implementation Person

-(void)setName
{
    NSLog(@"self %@", [self class]);
    NSLog(@"self %@", [super class]);
}

@end

@interface NewViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
