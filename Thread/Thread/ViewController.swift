//
//  ViewController.swift
//  Thread
//
//  Created by 王智刚 on 2020/9/17.
//  Copyright © 2020 王智刚. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        let a2 = Array((0...5).reversed())
//        Swift.min(<#T##x: Comparable##Comparable#>, <#T##y: Comparable##Comparable#>, <#T##z: Comparable##Comparable#>, <#T##rest: Comparable...##Comparable#>)
//        var arr = [1, 2,3]
//        arr.reverse()
//        arr.sorted(by: <)[0..<2]
//        arr.insert(<#T##newElement: Int##Int#>, at: <#T##Int#>)
//        Array(repeating: <#T##_#>, count: <#T##Int#>)
//        arr.last
//        var n: CLong
        let thread = Thread(target: self, selector: #selector(test), object: nil)
        thread.start()
        // Do any additional setup after loading the view.
    }
    
    @objc func test() {
        print("测试")
        print("当前线程名\(Thread.current.name ?? "")")
        DispatchQueue.main.sync {
            print("当前线程名\(Thread.current.name ?? "")")
        }
    }


}

//final class ThreadLocal<T> {
//
//    func set(_ value: T) {
//        let currentThread = Thread.current
//        let dict = currentThread.threadDictionary
//    }
//
//    func get() -> T? {
//        return nil
//    }
//
//    private func createMap(thread: Thread, val: T) {
//
//    }
//}

